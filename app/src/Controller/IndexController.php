<?php

namespace App\Controller;

use App\Repository\TaskRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(TaskRepository $repository): Response
    {
        $tasks = $repository->findSearch();
        return $this->render('index/index.html.twig', [
            'tasks' => $tasks
        ]);
    }
}
